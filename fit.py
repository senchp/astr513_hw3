import numpy as np
import emcee
import matplotlib.pyplot as plt
import corner

from confidence import find_confidence

def line(datx, angle, intercept):
    m = np.tan(angle)
    return (datx*m) + intercept

def lnlike(theta, x, y, xerr, yerr):
    """Likelihood function; see eqn 32 in Hogg+2010,
    and hw3 prob 1
    """
    mod = line(x, *theta)
    angle, intercept = theta
    m = np.tan(angle)
    exparg = (y-mod)**2. / ( (m**2 * xerr**2) + (yerr**2) )
    return -np.sum(exparg)

def lnprior(theta):
    """Simple flat prior.
    """
    angle, intercept = theta
    good = (-np.pi/2 < angle) and (angle < np.pi/2)
    good = good and (-np.inf < intercept) and (intercept < np.inf)
    if good:
        return 0.0
    else:
        return -np.inf

def lnprob(theta, x, y, xerr, yerr):
    """log-posterior likelihood function (to extremize).
    """
    lp = lnprior(theta)
    ll = lnlike(theta, x, y, xerr, yerr)
    if not np.isfinite(lp+ll):
        return -np.inf
    return lp + ll

def fit_line(xdat, ydat, e_xdat, e_ydat, guess=None,
        nwalkers=100, nsteps=1000, burn=250,
        tri_path=None, axis=None):
    """Performs a simple linear fit to x, y data with Guassian uncertainties
    in both directions.
        *dat: numpy arrays of length N
        guess: numpy array: [angle guess, intercept guess] (optional)
        tri_path: filepath to save triangle plot in (optional)
        axis: matplotlib axis on which to plot data/fit results

    Returns: emcee sampler chain
    """
    s = float((ydat[-1]-ydat[0]) / (xdat[-1]-xdat[0]))
    b = np.median(ydat) - (np.median(xdat)*s)

    if guess is None:
        guess = np.array([np.arctan(s), b])

    orders = guess * 0.01

    ndim = 2

    pos = [guess + (orders*np.random.randn(ndim)) for i in range(nwalkers)]

    sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob,
            args=(xdat, ydat, e_xdat, e_ydat))
    sampler.run_mcmc(pos, nsteps)

    chain = sampler.chain[:, burn:, :].reshape((-1, ndim))

    if tri_path is not None:
        trifig = corner.corner(chain, labels=[r"$\theta$", r"$b$"])
        trifig.savefig(tri_path)
        plt.clf()

    if axis is not None:
        axis.errorbar(xdat, ydat, xerr=e_xdat, yerr=e_ydat, color='b', zorder=10,
                ls='None', fmt='o', capsize=0)
        xs = np.linspace(xdat.min(), xdat.max(), 100)
        for i in np.random.randint(0, chain.shape[0], 100):
            theta = chain[i]
            axis.plot(xs, line(xs, *theta),
                    color='k', alpha=0.2, zorder=-1)
        ax.set_xlim(xdat.min(), xdat.max())
        ax.set_ylim(ydat.min(), ydat.max())

    return chain

if __name__=="__main__":
    import astropy.io.fits as fits

    tfdat = fits.getdata('data/reyes2011.fits')

    x = tfdat['rMAG']
    e_x = tfdat['e_rMAG']
    y = tfdat['V80c']
    e_y = tfdat['e_V80c']

    fig = plt.figure()
    ax = fig.add_subplot(111)

    res = fit_line(x, y, e_x, e_y,
            tri_path='figs/tri.png',
            axis=ax)

    ax.set_xlabel(r'$r$ (mag)')
    ax.set_ylabel(r'V80c (km/s)')
    fig.savefig('figs/fit.pdf', dpi=400, bbox_inches='tight')

    theta = res[:,0]
    slope = np.tan(theta)
    intercept = res[:,1]

    slope_ptile = np.percentile(slope, [16, 50, 84])
    intercept_ptile = np.percentile(intercept, [16, 50, 84])

    print("found slope median/16-84th errors: {0:.2f} -{1:.2f} +{2:.2f}"
            .format(slope_ptile[1],
                slope_ptile[1]-slope_ptile[0],
                slope_ptile[2]-slope_ptile[1]))

    con = find_confidence(slope, crange=0.684)
    print("found slope interval: {0:.2f} {1:.2f} {2:.2f}"
            .format(con[2],
                con[1]-con[2],
                con[0]-con[2]))

    print("found intercept median/16-84th errors: {0:.2f} -{1:.2f} +{2:.2f}"
            .format(intercept_ptile[1],
                intercept_ptile[1]-intercept_ptile[0],
                intercept_ptile[2]-intercept_ptile[1]))

    con = find_confidence(intercept, crange=0.684)
    print("found intercept interval: {0:.2f} {1:.2f} {2:.2f}"
            .format(con[2],
                con[1]-con[2],
                con[0]-con[2]))
